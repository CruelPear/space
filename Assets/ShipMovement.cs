using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMovement : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float moveSpeed = 30;
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");
        transform.Translate(new Vector3(horizontalInput, verticalInput, 0) * moveSpeed * Time.deltaTime);
        float z = -Input.GetAxis("Horizontal") * 50.0f;
        Vector3 euler = transform.localEulerAngles;
        float angle = transform.localEulerAngles.z;
        angle = (angle > 50) ? angle - 360 : angle;
        euler.z = angle;
        Debug.Log(euler);
        euler.z = Mathf.Lerp(euler.z, z,  2.0f * Time.deltaTime);
        transform.localEulerAngles = euler;
        //Debug.Log(euler);
    }
}


